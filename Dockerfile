FROM eclipse-temurin:17-jre-alpine
RUN addgroup -S testing && adduser -S testing -G testing
USER testing
COPY target/testing-0.0.1-SNAPSHOT.jar .
EXPOSE 9107
ENTRYPOINT ["sh","-c","java ${JAVA_OPTS} -jar testing-0.0.1-SNAPSHOT.jar "]