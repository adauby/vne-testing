package com.vneci.testing.dto.user;

public record UserDto(Long id, String name, String email) {

}
