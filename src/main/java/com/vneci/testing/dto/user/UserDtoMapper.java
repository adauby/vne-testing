package com.vneci.testing.dto.user;

import com.vneci.testing.repository.User;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class UserDtoMapper implements Function<User, UserDto> {
    /**
     * Applies this function to the given argument.
     *
     * @param o the function argument
     * @return the function result
     */
    @Override
    public UserDto apply(User user) {
        return new UserDto(user.getId(), user.getName(), user.getEmail());
    }
}
