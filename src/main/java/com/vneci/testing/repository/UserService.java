package com.vneci.testing.repository;

import com.vneci.testing.dto.user.UserDtoMapper;
import com.vneci.testing.dto.user.UserDto;
import com.vneci.testing.utils.exception.DataNotFoundExecption;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserDtoMapper userDtoMapper;
    private final UserRepository userRepository;


    public UserService(UserRepository userRepository, UserDtoMapper userDtoMapper) {
        this.userRepository = userRepository;
        this.userDtoMapper = userDtoMapper;
    }

    public UserDto findById(long id) {
        return userRepository.findById(id)
                .map(userDtoMapper)
                .orElseThrow(()-> new DataNotFoundExecption("User not found for id: "+id));
    }

    public Page<UserDto> findAll(PageRequest page) {
        return userRepository.findAll(page)
                .map(userDtoMapper);
    }

    public UserDto save(User user) {
        return userDtoMapper.apply(userRepository.save(user));
    }


    public UserDto update(long id, User user) {
        User userFound = userRepository.findById(id)
                .orElseThrow(()-> new DataNotFoundExecption("User not found for id: "+id));
        userFound.update(user);
        return userDtoMapper.apply(userRepository.save(userFound));
    }
}
