package com.vneci.testing.controller;

import com.vneci.testing.dto.user.UserDto;
import com.vneci.testing.repository.UserService;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class UserController {

    public static final String H_PAGE = "x-page";
    public static final String H_TOTAL_ELEMENTS = "x-all";
    public static final String H_TOTAL_PAGES = "x-pages";
    public  static final String H_HAS_NEXT_PAGE = "x-has-next-page";
    private final UserService userService;
    private final UserPayloadMapper userMapper;

    public UserController(UserService userService, UserPayloadMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping(value = "/users/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto>findUser(@PathVariable long id){
        var user = userService.findById(id);
        return ResponseEntity.ok(user);
    }

    @GetMapping(path = "/users")
    public ResponseEntity<List<UserDto>>getUsers(
            @RequestParam(defaultValue = "0") int page
            ,@RequestParam(defaultValue = "50")int size){
        var users = userService.findAll(PageRequest.of(page,size));
        if(!users.hasContent())
            return ResponseEntity
                    .noContent()
                    .build();

        return ResponseEntity.ok()
                .header(H_PAGE, String.valueOf(users.getNumber()))
                .header(H_TOTAL_ELEMENTS, String.valueOf(users.getTotalElements()))
                .header(H_TOTAL_PAGES, String.valueOf(users.getTotalPages()))
                .header(H_HAS_NEXT_PAGE, String.valueOf(users.hasNext()))
                .body(users.getContent());

    }

    @PostMapping(path = "/users", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<UserDto>createUser(@RequestBody UserRequestBody newUser){
        var user = userService.save(userMapper.apply(newUser));
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(user);
    }

    @PutMapping(path = "/users/{id}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    ResponseEntity<UserDto> updateUser(@PathVariable long id, @RequestBody UserRequestBody userUpdate){
        var user = userService.update(id, userMapper.apply(userUpdate));
        return ResponseEntity.ok(user);
    }

}
