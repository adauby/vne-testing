package com.vneci.testing.controller;

import com.vneci.testing.utils.exception.DataNotFoundExecption;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

@RestControllerAdvice
@Log4j2
public class ControllerAdvisor {
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataNotFoundExecption.class)
    public ResponseError handleNoDataFound(DataNotFoundExecption ex, WebRequest request){
        HttpServletRequest req = ((ServletWebRequest)request).getRequest();
        return new ResponseError(req.getRequestURI(),ex.getMessage(),"timestamp",HttpStatus.NOT_FOUND.value());
    }


}
