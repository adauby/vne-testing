package com.vneci.testing.controller;

public record ResponseError(String path,String message,String timestamp,int status) {
}
