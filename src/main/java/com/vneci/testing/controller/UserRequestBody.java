package com.vneci.testing.controller;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public record UserRequestBody(@NotBlank String name
        , @Email String email
        , @NotBlank String password) {
}
