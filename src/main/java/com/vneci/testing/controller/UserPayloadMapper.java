package com.vneci.testing.controller;

import com.vneci.testing.repository.User;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class UserPayloadMapper implements Function<UserRequestBody, User> {
    /**
     * Applies this function to the given argument.
     *
     * @param userRequestBody the function argument
     * @return the function result
     */
    @Override
    public User apply(UserRequestBody data) {
        return new User(data.name(), data.email(), data.password());
    }
}
