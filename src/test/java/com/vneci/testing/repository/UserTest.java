package com.vneci.testing.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserTest {

    @Test
    void testUpdate(){
        var user = new User("Assi Mason","aadauby@vne-ci.com","passwd");
        user.setId((long)1);

        var update = new User("Ollélé Mathurin","amthurin@vne-ci.com", "passwd2");
        user.update(update);
        assertEquals("Ollélé Mathurin",user.getName());
        assertEquals("amthurin@vne-ci.com", user.getEmail());
        assertEquals("passwd2",user.getPassword());
        assertEquals((long)1,user.getId());
    }
}
