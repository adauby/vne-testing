package com.vneci.testing.repository;

import com.vneci.testing.dto.user.UserDto;
import com.vneci.testing.dto.user.UserDtoMapper;
import com.vneci.testing.utils.exception.DataNotFoundExecption;
import lombok.extern.log4j.Log4j2;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.Optional;

import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
@Log4j2
class UserServiceTest {

    @Mock
    UserRepository userRepository;
    @Mock
    UserDtoMapper userDtoMapper;
    @InjectMocks
    UserService userService;

    @Test
    void testFindById(){
        Long uid = (long)1;

        User user = new User(uid,"Ollélé Mathurin","amthurin@vne-ci.com"
                , "passwd2");
        BDDMockito.given(userDtoMapper.apply(user)).willReturn(new UserDto(user.getId()
                ,user.getName(), user.getEmail()));
        BDDMockito.given(userRepository.findById(uid))
                .willThrow(DataNotFoundExecption.class);
        UserDto userFound = userService.findById(uid);
        assertEquals("Ollélé Mathurin",userFound.name());
        assertEquals("amthurin@vne-ci.com", userFound.email());
        assertEquals(uid, userFound.id());

    }

    @Test
    void testThrowingAssertion(){
        long uid = (long)1;
        BDDMockito.given(userRepository.findById(uid))
                .willThrow(DataNotFoundExecption.class);
        var msg = assertThrows(DataNotFoundExecption.class, ()-> userService.findById(uid));
    }



}
