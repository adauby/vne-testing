package com.vneci.testing.controller;

import com.vneci.testing.dto.user.UserDto;
import com.vneci.testing.repository.UserService;
import com.vneci.testing.utils.exception.DataNotFoundExecption;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest({UserController.class, UserPayloadMapper.class})
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    UserService userService;
    @Test
    void testOkOnFindById() throws Exception{
        BDDMockito.given(userService.findById((long)2))
                .willReturn(new UserDto((long)2,"Soumahoro", "shamed@vne-ci.com"));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users/{id}",2)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Soumahoro")))
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.email", is("shamed@vne-ci.com")));
    }

    @Test
    void testNotFoundOnFindById() throws Exception{
        BDDMockito.given(userService.findById((long)101)).willThrow(new DataNotFoundExecption("User not found for id: "+ 101));
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}",(long)101)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.path", Matchers.is("/users/101")))
                .andExpect(jsonPath("$.message", Matchers.is("User not found for id: 101")))
                .andExpect(jsonPath("$.timestamp", Matchers.notNullValue()))
                .andExpect(jsonPath("$.status", Matchers.is(404)));
    }
    @Test
    void testOkOnGetUsers() throws Exception{
       var listOfUser = List.of(
               new UserDto((long)1,"n1","email@vne-ci.com")
               ,new UserDto((long)1,"n2","email2@vne-ci.com"));
        BDDMockito.given(userService.findAll(PageRequest.of(2,25)))
                .willReturn(new PageImpl<>(listOfUser, PageRequest.of(2,25),4));

        mockMvc.perform(MockMvcRequestBuilders.get("/users")
                .param("page","2")
                .param("size", "25"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is("n1")))
                .andExpect(header().stringValues(UserController.H_PAGE, "2"))
                .andExpect(header().stringValues(UserController.H_HAS_NEXT_PAGE, "false"));
    }



}