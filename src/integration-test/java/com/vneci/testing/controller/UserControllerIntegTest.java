package com.vneci.testing.controller;

import com.vneci.testing.dto.user.UserDto;
import com.vneci.testing.repository.User;
import com.vneci.testing.repository.UserRepository;
import com.vneci.testing.repository.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerIntegTest {
    private static Logger logger = LoggerFactory.getLogger(UserControllerIntegTest.class);
    @LocalServerPort
    private int port;

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    private final  String BASE_URL = "http://localhost:";

    static MariaDBContainer<?> mariaDBContainer = new MariaDBContainer<>("mariadb:10.7.8")
            .withLogConsumer(new Slf4jLogConsumer(logger));

    @DynamicPropertySource
    static void mariadbProperties(DynamicPropertyRegistry registry){
        mariaDBContainer.start();
        registry.add("spring.datasource.url",mariaDBContainer::getJdbcUrl);
        registry.add("spring.datasource.driverClassName",mariaDBContainer::getDriverClassName);
        registry.add("spring.datasource.username", () -> mariaDBContainer.getUsername());
        registry.add("spring.datasource.password", () -> mariaDBContainer.getPassword());
    }

    @AfterEach
    public void cleanDataBase(){
        userRepository.deleteAll();
    }

    @Test
    void testFindUser() throws Exception{
        var users = userRepository.saveAll(List.of(new User("name one","email1@email.com","passwd")
                ,new User("name two","email2@email.com","passwd")
                ,new User("name two","email3@email.com","passwd")));
        var user = users.get(0);
        ResponseEntity<UserDto>response = restTemplate.getForEntity(createUrl("/users/"+user.getId()),UserDto.class);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());


    }

    private String createUrl(String uri) {
        return BASE_URL+port+uri;
    }
}
